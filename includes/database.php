<?php

require_once('config.php');

class MySQLDatabase extends mysqli {

  public $last_query;
  private $magic_quotes_active;

  function __construct() {
    // $this->connection;
    parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_error()) {
      die("Connect error" . mysqli_connect_error());
    }

    $this->magic_quotes_active = get_magic_quotes_gpc();
  }

  public function query($sql) {
    $this->last_query = $sql;
    $result = parent::query($sql);
    $this->confirm_query($result);
    return $result;
  }

  public function escape_value($value) {
    if ($this->magic_quotes_active)
      stripslashes($value);
    return $this->real_escape_string($value);
  }

  // public function find_all($table = '') {
  //   $sql = "SELECT * FROM {$table}";
  //   $result = $this->query($sql);
  //   $objects = array();
  //   while ($obj = $result->fetch_object())
  //     array_push($objects, $obj);
  //   return $objects;
  //   $obj->close();
  // }

  // public function find_by_id($table = '', $id = 0) {
  //   $sql = "SELECT * FROM {$table} WHERE id = {$id} LIMIT 1";
  //   $result = $this->query($sql);
  //   return $result->fetch_object();
  // }

  private function confirm_query($result) {
    if (!$result) {
      $msg  = "Database query failed: " . $this->error . "<br>";
      // comment this out when in production
      $msg .= "Last SQL query: " . $this->last_query;
      die($msg);
    }
  }
}

$db = new MySQLDatabase();

?>

