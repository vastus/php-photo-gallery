<?php
require_once('database.php');

class User {

  public $id;
  public $username;
  public $password;
  public $first_name;
  public $last_name;

  private static function pluralize() {
    $name = get_class();
    // self::$pluralized = strtolower($name) . 's';
    return strtolower($name) . 's';
  }

  public static function find_all() {
    return self::find_by_sql("SELECT * FROM users");
  }

  public static function find_by_id($id) {
    $sql = "SELECT * FROM users WHERE id = {$id} LIMIT 1";
    $results = self::find_by_sql($sql);
    return empty($results) ? false : array_shift($results);
  }

  public static function find_by_sql($sql) {
    global $db;
    $result = $db->query($sql);
    $objects = array();
    while ($object = $result->fetch_object()) {
      array_push($objects, self::instantiate($object));
    }
    return $objects;
  }

  public function full_name() {
    return $this->first_name . " " . $this->last_name;
  }
  
  private static function instantiate($record) {
    $obj = new self;
    foreach($record as $attribute=> $value) {
      if ($obj->has_attribute($attribute)) {
        $obj->$attribute = $value;
      }
    }
    return $obj;
  }

  private function has_attribute($attribute) {
    $object_vars = get_object_vars($this);
    return array_key_exists($attribute, $object_vars);
  }
}

?>
