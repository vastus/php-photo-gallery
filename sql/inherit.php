<?php


class Vanhempi {
  public function sanoo() {
    return "vanahempi";
  }
}

class Lapsi extends Vanhempi {

}

$lapsi = new Lapsi();
echo $lapsi->sanoo();

$class_name = get_class($lapsi);
echo strtolower($class_name), 's';

?>
