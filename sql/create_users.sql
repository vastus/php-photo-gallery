CREATE TABLE IF NOT EXISTS users (
  id int(11) NOT NULL auto_increment,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(40) NOT NULL,
  first_name VARCHAR(30) NOT NULL,
  last_name VARCHAR(30) NOT NULL,
  PRIMARY KEY (id)
);
